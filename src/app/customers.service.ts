import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CustomersService {
  customers = [{name:'gal', years:20, income:30000}, {name:'gal', years:20, income:30000}];

  customerCollection:AngularFirestoreCollection;
  userCollection:AngularFirestoreCollection = this.db.collection('users'); 

  public getCustomers(userId){
    this.customerCollection = this.db.collection(`users/${userId}/customers`);
    return this.customerCollection.snapshotChanges().pipe(map(
      collection =>collection.map(
        document => {
          const data = document.payload.doc.data();
          data.id = document.payload.doc.id;
          return data;
        }
      )
    ))
  }


  addCustomer(userId:string,name:string,years:number, income:number, prob:string, pressSave:number){
    const customer = {name:name, years:years, income:income, prob:prob, pressSave:pressSave}; 
    this.userCollection.doc(userId).collection('customers').add(customer);
  }


  deleteCustomer(Userid:string, id:string){
    this.db.doc(`users/${Userid}/customers/${id}`).delete(); 
  } 

  updateCustomer(userId:string,id:string,name:string,years:number, income:number){
    this.db.doc(`users/${userId}/customers/${id}`).update(
      {
        name:name,
        years:years,
        income:income
      }
    )
  }

  save(userId:string,id:string,name:string,years:number,income:number,prob:string,pressSave:number){
    this.db.doc(`users/${userId}/customers/${id}`).update(
      {
        name:name,
        years:years,
        income:income,
        prob:prob,
        pressSave:pressSave
      }
    )
  }



  constructor(private db:AngularFirestore) { }
}