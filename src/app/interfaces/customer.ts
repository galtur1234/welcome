export interface Customer {
    id:string,
    name:string,
    years:number,
    income:number,
    prob?:string,
    pressSave?:number



}
